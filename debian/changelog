podcastparser (0.6.8-2) unstable; urgency=medium

  * Use pytest for tests (Closes: #1018438)
  * Bump Standards-Version to 4.6.1

 -- tony mancill <tmancill@debian.org>  Wed, 31 Aug 2022 12:43:20 -0700

podcastparser (0.6.8-1) unstable; urgency=medium

  * New upstream version 0.6.8
  * Add build-dep on dh-sequence-python3

 -- tony mancill <tmancill@debian.org>  Sun, 10 Oct 2021 21:32:58 -0700

podcastparser (0.6.7-1) unstable; urgency=medium

  * New upstream version 0.6.7
  * Bump Standards-Version to 4.6.0
  * Freshen debian/copyright

 -- tony mancill <tmancill@debian.org>  Tue, 10 Aug 2021 21:48:43 -0700

podcastparser (0.6.5-1) unstable; urgency=medium

  * New upstream version 0.6.5
  * Bump Standards-Version to 4.5.0
  * Use debhelper-compat 13
  * Set "Rules-Requires-Root: no" in debian/control

 -- tony mancill <tmancill@debian.org>  Mon, 06 Jul 2020 10:21:44 -0700

podcastparser (0.6.4-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * d/control: Fix wrong Vcs-*

  [ tony mancill ]
  * New upstream version 0.6.4
  * Drop python-podcastparser (python 2) binary package (Closes: #937305)
  * Bump Standards-Version to 4.4.0
  * Use debhelper 12
  * Freshen years in debian/copyright
  * python-podcastparser-doc is now marked Multi-Arch: foreign
  * Replace dpkg-parsechangelog with SOURCE_DATE_EPOCH
  * Add Build-Depends on cmake

 -- tony mancill <tmancill@debian.org>  Mon, 02 Sep 2019 12:21:47 -0700

podcastparser (0.6.3-1) unstable; urgency=medium

  * New upstream version 0.6.3
  * Update Vcs fields for migration from Alioth -> Salsa
  * Bump Standards-Version to 4.1.4
  * Freshen debian/copyright

 -- tony mancill <tmancill@debian.org>  Sun, 22 Apr 2018 09:56:47 -0700

podcastparser (0.6.2-1) unstable; urgency=medium

  * New upstream version 0.6.2
  * Bump Standards-Version to 4.1.3
  * Freshen dates in debian/copyright
  * Drop build-dep on python-sphinx

 -- tony mancill <tmancill@debian.org>  Mon, 01 Jan 2018 16:20:27 -0800

podcastparser (0.6.1-1) unstable; urgency=medium

  * New upstream version.
  * Correct the Vcs URLs.

 -- tony mancill <tmancill@debian.org>  Mon, 26 Dec 2016 19:45:52 -0800

podcastparser (0.6.0-1) unstable; urgency=medium

  * Initial release (Closes: #847046)

 -- tony mancill <tmancill@debian.org>  Mon, 05 Dec 2016 23:05:06 -0800
